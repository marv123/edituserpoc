import React, { Component } from 'react';
import {Provider} from 'react-redux';
import configureStore from './redux/configureStore';
import {injectGlobal} from 'styled-components';
import UserEditContainer from './components/containers/editUser';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

const store = configureStore();

injectGlobal`
  @@font-face {
      font-family: 'Roboto', sans-serif;
  }
  body {
    background-color: white;
    height: 100vh;
    margin: 0;
    padding: 0;
  }
`


class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <MuiThemeProvider>
          <UserEditContainer/>
        </MuiThemeProvider>
      </Provider>
    );
  }
}

export default App;
