import {combineReducers} from 'redux';
import {
  REQUEST_USER,
  RECEIVE_USER,
  CHANGE_USER_FIRST_NAME,
  CHANGE_USER_LAST_NAME,
  CHANGE_USER_PASSWORD,
  CHANGE_USER_EMAIL,
  CHANGE_USER_PASSWORD_REPEAT,
  RESET_USER_CHANGES,
  CHANGE_USER_USERNAME,
  SAVING_USER,
} from './actions';

function userReducer(state = {
  user: null,
  isFetching: false,
  didInvalidate: false,
  userTemplate: {
    firstName: '',
    lastName: '',
    email: '',
    password: '',
    passwordRepeat: '',
    username: '',
  },
  valid: false,
  validationMessages: {},
  passwordMatch: true,
}, action) {
  switch (action.type) {
    case REQUEST_USER:
      return {
        ...state,
        isFetching: true,
      }
    case RECEIVE_USER:
      let user;
      if(action.user) {
        user = {...action.user, password: '', passwordRepeat: ''}
      }
      else{
        user = state.userTemplate;
      }
      return {
        ...state,
        user,
        isFetching: false,
        userTemplate: user,
        valid: true,
        validFields: createValidObject(user),
        new: !action.user,
      }
    case SAVING_USER:
      return {
        ...state,
        isFetching: true,
      }
    case CHANGE_USER_FIRST_NAME:
      return {
        ...state,
        user: {...state.user, firstName: action.firstName},
        validFields: {...state.validFields, [action.name]: action.valid},
        validationMessages: {...state.validationMessages, [action.name]: {show: !action.valid, message: 'First name is not valid'}}
      }
    case CHANGE_USER_LAST_NAME:
      return {
        ...state,
        user: {...state.user, lastName: action.lastName},
        validFields: {...state.validFields, [action.name]: action.valid},
        validationMessages: {...state.validationMessages, [action.name]: {show: !action.valid, message: 'Last name is not valid'}}
      };
    case CHANGE_USER_EMAIL:
      return {
        ...state,
        user: {...state.user, email: action.email},
        validFields: {...state.validFields, [action.name]: action.valid},
        validationMessages: {...state.validationMessages, [action.name]: {show: !action.valid, message: 'Email is not valid'}}
      };
    case CHANGE_USER_PASSWORD:
      return {
        ...state,
        user: {...state.user, password: action.password},
        validFields: {...state.validFields, [action.name]: action.valid},
        validationMessages: {...state.validationMessages, [action.name]: {show: !action.valid, message: 'Password name is not valid'}}
      };
    case CHANGE_USER_PASSWORD_REPEAT:
      return {
        ...state,
        user: {...state.user, passwordRepeat: action.passwordRepeat},
        validFields: {...state.validFields, [action.name]: action.valid},
        validationMessages: {...state.validationMessages, [action.name]: {show: !action.valid, message: 'Password name is not valid'}}
      };
    case CHANGE_USER_USERNAME:
      return {
        ...state,
        user: {...state.user, username: action.username},
        validFields: {...state.validFields, [action.name]: action.valid},
        validationMessages: {...state.validationMessages, [action.name]: {show: !action.valid, message: 'Username name is not valid'}}
      };

    case RESET_USER_CHANGES:
      return {
        ...state,
        user: state.userTemplate,
      }
    default:
      return state;
  }
}

function validatePassword(password, passwordRepeat) {
  return password === passwordRepeat;
}

function createValidObject(user) {
  const validFields = {};
  for(let [key] of Object.entries(user)) {
    validFields[key] = true;
  }
  return validFields;
}

const rootReducer = combineReducers({
  user: userReducer,
});

export default rootReducer;
