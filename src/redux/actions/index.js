import fetch from 'isomorphic-fetch';

export const REQUEST_USER = 'REQUEST_USER';
export const RECEIVE_USER = 'RECEIVE_USER';
export const CHANGE_USER_FIRST_NAME = 'CHANGE_USER_FIRST_NAME';
export const CHANGE_USER_LAST_NAME = 'CHANGE_USER_LAST_NAME';
export const CHANGE_USER_EMAIL = 'CHANGE_USER_EMAIL';
export const CHANGE_USER_PASSWORD = 'CHANGE_USER_PASSWORD';
export const CHANGE_USER_PASSWORD_REPEAT = 'CHANGE_USER_PASSWORD_REPEAT';
export const CHANGE_USER_USERNAME = 'CHANGE_USER_USERNAME';
export const RESET_USER_CHANGES = 'RESET_USER_CHANGES';
export const SAVING_USER = 'SAVING_USER';

export function requestUser(userId) {
  return {
    type: REQUEST_USER,
    userId,
  }
}

export function receiveUser(user) {
  return {
    type: RECEIVE_USER,
    user: user,
  }
}

function fetchUser() {
  return dispatch => {
    let memberId = document.getElementById('userId').dataset.value;
      dispatch(requestUser(memberId));
    let getUrl = document.getElementById('getUrl').dataset.value;
    return fetch(`${getUrl}`, {
      method: 'POST',
      body: JSON.stringify({userId: memberId}),
      credentials: "same-origin"
    })
    .then(response => response.json())
    .then(json => dispatch(receiveUser(json.data.user)));
  }
}

function shouldFetchUser(state) {
    return !state.user.user;
}

function savingUser() {
  return {
    type: SAVING_USER,
  }
}

function saveUser(user, newUser) {
  return dispatch => {
    dispatch(savingUser())
    const saveUrl = document.getElementById('saveUrl').dataset.value;
    const payload = {
      ...user,
      newUser,
    }
    return fetch(`${saveUrl}`, {
      method: 'POST',
      body: JSON.stringify(payload),
      credentials: "same-origin",
    })
    .then(response => response.json())
    .then(json => dispatch(receiveUser(json.data.user)));
  }
}

export function postUser(user, newUser) {
  return (dispatch, getState) => {
    return dispatch(saveUser(user, newUser));
  }
}

export function fetchUserIfNeeded() {
    return (dispatch, getState) => {
      if(shouldFetchUser(getState())) {
        return dispatch(fetchUser());
      }
    }
}

export function changeUserFirstName(firstName, valid, name) {
  return {
      type: CHANGE_USER_FIRST_NAME,
      firstName,
      valid,
      name
  }
}

export function changeUserLastName(lastName, valid, name) {
  return {
    type: CHANGE_USER_LAST_NAME,
    lastName,
    valid,
    name
  }
}

export function changeUserEmail(email, valid, name) {
  return {
    type: CHANGE_USER_EMAIL,
    email,
    valid,
    name
  }
}

export function changeUserPassword(password, valid, name) {
  return {
    type: CHANGE_USER_PASSWORD,
    password,
    valid,
    name
  }
}

export function changeUserPasswordRepeat(passwordRepeat, valid, name) {
  return {
    type: CHANGE_USER_PASSWORD_REPEAT,
    passwordRepeat,
    valid,
    name
  }
}

export function changeUserUsername(username, valid, name) {
  return {
    type: CHANGE_USER_USERNAME,
    username,
    valid,
    name,
  }

}

export function resetUserChanges() {
  return {
    type: RESET_USER_CHANGES,
  }
}
