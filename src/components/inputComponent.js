import React, {Component} from 'react';
import PropTypes from 'prop-types';
import FormInput, {InvalidMessage} from './styledComponents/FormInput';

class InputComponent extends Component{
  constructor(props) {
    super(props);
    this.state = {
      valid: true,
    }
  }
  onChange = (value) => {
  let valid = true;
  if(this.props.validationRegExp) {
    valid = new RegExp(this.props.validationRegExp).test(value);
    this.setState({
      valid: valid,
    });
  }
  this.props.onChange(value, valid, this.props.name);
  }

  getWarning = () => {
    if(this.props.required && this.props.value === '') return <InvalidMessage>This field is required</InvalidMessage>;
    if(!this.state.valid) return <InvalidMessage>Value is not valid</InvalidMessage>
    return null;
  }

  render() {
    return(
      <tr>
          <td>
            {this.props.label}
          </td>
          <td>
              <FormInput
                name={this.props.name}
                type={this.props.type}
                value={this.props.value}
                onChange={(event) => this.onChange(event.target.value)}
                required={this.props.required}
              />
          </td>
          <td>
            {this.getWarning()}
          </td>
      </tr>
    )
  }
}

export default InputComponent;

InputComponent.propTypes = {
  type: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  validationRegExp: PropTypes.string,
  required: PropTypes.bool,
}
