import styled from 'styled-components';

export default styled.input`
  outline: none;
  margin-left: 10px;
  margin-right: 10px;
  border-radius: 5px;
  padding: 4px;
  &:focus{
    box-shadow: ${props => (props.required && props.value.length === 0) ? '0 0 0 0.2rem rgba(220,53,69,.25)' : 'initial'};
  }
`;

export const InvalidMessage = styled.label`
  color: #B70000;
  font-weight: 400;
  letter-spacing: 1px;
  font-size: 13px;
`;
