import React, {Component} from 'react';
import InputComponent from './inputComponent';
import RaisedButton from 'material-ui/RaisedButton';
import {Flex} from './styledComponents/Containers';

import { bindActionCreators } from 'redux'
import {connect} from 'react-redux';
import {
  changeUserFirstName,
  changeUserLastName,
  changeUserEmail,
  changeUserPassword,
  changeUserPasswordRepeat,
  resetUserChanges,
  changeUserUsername,
} from '../redux/actions';


class UserForm extends Component{

  enableSaveButton = () => {
    for(let [key, value] of Object.entries(this.props.validFields)) {
      if(!value) return true;
    }
    if(this.props.newUser) {
      return (this.props.user.password.length === 0 || this.props.user.passwordRepeat.length === 0);
    }
    return false;
  }

  render() {
    return(
      <div>
        <form>
          <fieldset style={{minWidth: '500px'}}>
            <legend>
              User information
            </legend>
            <table>
              <tbody>
                <InputComponent
                  name='firstName'
                  label='firstName'
                  type='text'
                  value={this.props.user.firstName}
                  onChange={this.props.changeUserFirstName}
                  validationRegExp='^[a-zA-Z]+$'
                  required
                />
                <InputComponent
                  name='lastName'
                  label='lastName'
                  type='text'
                  value={this.props.user.lastName}
                  onChange={this.props.changeUserLastName}
                  validationRegExp='^[a-zA-Z]+$'
                  required
                />
                  <InputComponent
                    name='username'
                    label='userName'
                    type='text'
                    value={this.props.user.username}
                    onChange={this.props.changeUserUsername}
                    validationRegExp='^[a-zA-Z0-9]+$'
                    required
                  />
                <InputComponent
                  name='email'
                  label='email'
                  type='email'
                  value={this.props.user.email}
                  onChange={this.props.changeUserEmail}
                  validationRegExp='^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$'
                  required
                />
                <InputComponent
                  name='password'
                  label='password'
                  type='password'
                  value={this.props.user.password}
                  onChange={this.props.changeUserPassword}
                  validationRegExp='^[_0-9a-z]*$'
                  required
                />
                <InputComponent
                  name='passwordRepeat'
                  label='repeat password'
                  type='password'
                  value={this.props.user.passwordRepeat}
                  onChange={this.props.changeUserPasswordRepeat}
                  validationRegExp='^[_0-9a-z]*$'
                  required
                />
              </tbody>
            </table>
            <hr></hr>
            <Flex horizintalCenter>
              <RaisedButton onClick={this.props.saveUser} primary disabled={this.enableSaveButton()} label='Save'/>
              <RaisedButton onClick={this.props.resetUserChanges} style={{marginRight: 'auto'}} label='Reset'/>
              <RaisedButton label='Cancel'/>
            </Flex>
          </fieldset>
        </form>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    valid: state.user.valid,
    validFields: state.user.validFields,
    newUser: state.user.new,
  }
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators(
        {
          changeUserFirstName,
          changeUserLastName,
          changeUserEmail,
          changeUserPassword,
          changeUserPasswordRepeat,
          resetUserChanges,
          changeUserUsername,
        }, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(UserForm);
