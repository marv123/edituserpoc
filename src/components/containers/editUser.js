import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  fetchUserIfNeeded,
  postUser,
} from '../../redux/actions';

import Paper from 'material-ui/Paper';
import CircularProgress from 'material-ui/CircularProgress';
import {Flex} from '../styledComponents/Containers';
import UserEdit from '../userFormComponent';

class UserEditContainer extends Component{

    constructor(props) {
      super(props);
      this.state = {
        errors: [],
      }
    }

    componentWillMount() {
        const {dispatch} = this.props;
        dispatch(fetchUserIfNeeded());
    }

    saveUser = () => {
      if(this.props.user.password === this.props.user.passwordRepeat) {
        console.log('should submit');
        const {dispatch, user, newUser} = this.props;
        dispatch(postUser(user, newUser));
      } else {
        this.setState({errors: [
          {message: 'Passwords are not matching'}
        ]});
      }
    }

    renderErros = () => {
      return this.state.errors.map((error) => {
        return <li>{error.message}</li>
      });
    }

    render() {
      if(!this.props.user) {
        return (
          <Flex column verticalCenter horizintalCenter>
            <Paper zDepth={1} rounded={false}>
              <CircularProgress size={80} thickness={5}/>
            </Paper>
          </Flex>
        )
      }
      return(
        <Flex column verticalCenter horizintalCenter>
            <ul>
              {this.renderErros()}
            </ul>
            <UserEdit saveUser={this.saveUser} user={this.props.user}/>
        </Flex>
      )
    }
}

function mapStateToProps(state) {
  const {
    isFetching,
    user,
  } = state.user;

  return {
    isFetching,
    user,
    newUser: state.new,
  }
}

export default connect(mapStateToProps)(UserEditContainer);
